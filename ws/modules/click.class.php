<?php

include_once "dbUtils.class.php";
include_once "visitor.class.php";

class Click {
    private $id;
    private $position;
    private $datetime;    
    private $ip;
    private $sql;
    
    function __construct($position) {
        $this->position = $position;
        $this->ip = Visitor::GetUserIp();
        $this->sql = new MySqlUtils();
        $this->datetime = (new DateTime())->format('Y-m-d H:i:s');
    }

    public function __destruct() {
        
    }
    
    public function addClick(){        
        return $this->sql->Query(
                "insert into click(ip,position,datetime)"
                . "values('$this->ip','$this->position','$this->datetime')"
        );   
    }
}