<?php

include_once "./config.php";

class MySqlUtils {

    //Connection String Settings
    private $dbname = Conf::DB_NAME;
    private $dbuser = Conf::DB_USER;
    private $dbpwd = Conf::DB_PASS;
    private $dbhost = Conf::DB_HOST;
    private $conn;
    
    // DB Instances
    private $DBH;
    private $DB;
    
    public function __construct() {
        $this->Connect();
    }

    // Connects to DB Server
    private function Connect() {
        $this->conn = new mysqli($this->dbhost, $this->dbuser, $this->dbpwd, $this->dbname);
        if ($this->conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }         
        
    }
    
    // Executes a query and get the results
    public function Query($query_text) {
        // Run the query
        //echo $query_text;        
        $res =  $this->conn->query($query_text);
        $this->conn->close();
        return $res;
    }
	
}
