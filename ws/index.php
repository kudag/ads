<?php
    include_once "modules/click.class.php";
    include_once "modules/scroll.class.php";

    $pos = $_GET['pos'];
    $action = $_GET['action'];
    $resp = "";
     
     if($action === 'click'){
        $click = new Click($pos);
        $resp = $click->addClick();
     }else if($action === 'scroll'){
         $scroll = new Scroll($pos);
         $resp = $scroll->addScroll();
     }else{
         $resp = ['error' => 'Unknown action'];
     }

header('Content-Type: application/json');
if($resp === false){
    $resp = ['error' => 'An error occurred'];
}if($resp === true){
    $resp = ['success' => 'Action added'];
}

echo json_encode($resp);