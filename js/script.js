(function($){
    $(document).ready(function(){
        $('body').click(function(e){
            var pos = event.pageX + "," + event.pageY;  
            sendData('click', pos);        
        });

        $('body').on("mousewheel", function() {
            sendData('scroll', $(document).scrollTop()); 
        });
        
        showDate();
    
    });
    
    function sendData(action, pos){
        $.get("ws?action="+action+"&pos="+pos, function(data) {
            console.log(data);
        });
    }
    
    function showDate(){
        var date = new Date();
        var html = "<div class='date'>" + date.toLocaleDateString() + "</div>";
        
        //check if widget is loaded
        var checkWidget = setInterval(function(){
            //div.sm-widget:nth-child(3) seem not to exist in the dom though
            if($('div.sm-widget:nth-child(3)').length > 0){
                $('div.sm-widget:nth-child(3)').append(html);
            }            
            //if div.sm-widget:nth-child(3)  does not exist add the date to the first div.sm-widget
            else if($('div.sb-widget:nth-child(1)').length > 0)         
                $('div.sb-widget:nth-child(1)').append(html);             
            
            if($('div.sb-widget .date').length > 0)
                clearInterval(checkWidget); 
            
        }, 500);
    }
    
})(jQuery)